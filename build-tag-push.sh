#!/bin/bash
set -e;
echo "###############"
echo "Fetching Latest files from IntelOwl"
echo "###############"
cp IntelOwl/docker/entrypoint_uwsgi.sh .
cp IntelOwl/configuration/intel_owl.ini .
cp IntelOwl/docker/Dockerfile .
cp IntelOwl/docker/Dockerfile_nginx ./dockerfile-nginx
echo "###############"
echo "Modifying files for K8S Deployment"
echo "###############"
sed -i -e 's:requirements/:IntelOwl/requirements/:g' -e 's:COPY . :COPY IntelOwl/. :g' Dockerfile
{
echo "
RUN mkdir -p /etc/uwsgi/sites && mv \${PYTHONPATH}/configuration/intel_owl.ini /etc/uwsgi/sites/"
echo "COPY ./IntelOwl/configuration/* /opt/deploy/configuration/"
} >> ./Dockerfile
{
echo "
COPY IntelOwl/configuration/nginx/http.conf /etc/nginx/conf.d/default.conf"
echo "COPY IntelOwl/configuration/nginx/errors.conf /etc/nginx/errors.conf"
echo "EXPOSE 80 443"
} >> ./dockerfile-nginx
echo "###############"
echo "Building, Tagging, and Pushing Images to Docker Registry: $1"
echo "###############"
docker build --tag "$1"/intel-owl:latest .
docker push "$1"/intel-owl:latest
sed -i -e 's/server uwsgi/server backend-webapp-service.default.svc.cluster.local/g' IntelOwl/configuration/nginx/http.conf
sed -i -e "s/changeit/$1/g" ./dockerfile-celery-beat ./dockerfile-celery-worker ./dockerfile-nginx
docker build --tag "$1"/celery-beat:latest -f dockerfile-celery-beat .
docker push "$1"/celery-beat:latest
docker build --tag "$1"/celery-worker:latest -f dockerfile-celery-worker .
docker push "$1"/celery-worker:latest
docker build --tag "$1"/nginx-rp:latest -f ./dockerfile-nginx .
docker push "$1"/nginx-rp:latest
docker build --tag "$1"/rabbitmq:latest -f dockerfile-rabbitmq .
docker push "$1"/rabbitmq:latest
echo "###############"
echo "Updating Docker Repo in deployment files"
echo "###############"
sed -i -e "s/<docker-repo>/$1/g" ./backend-webapp.yaml ./frontend-deployment.yaml ./rabbitmq-daemon.yaml
